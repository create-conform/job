/////////////////////////////////////////////////////////////////////////////////////////////
//
// job
//
//    Job class used for standardizing task execution.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Privates
//
/////////////////////////////////////////////////////////////////////////////////////////////
var Error = require("error");
var type =  require("type");
var Task =  require("task");

var ERROR_INVALID_JOB = "Invalid Job";

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Job Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function Job(obj) {
    var self = this;

    this.user = null;
    this.created = null;
    this.scheduled = null;
    this.started = null;
    this.ended = null;
    this.description = null;
    this.parent = null;
    this.origin = null;

    this.tasks = [];
    this.parameters = {};

    if (obj) {
        //
        // parse user
        //
        if (!type.isString(obj.user) && typeof obj.user !== "undefined" && obj.user !== null) {
            throw new Error(ERROR_INVALID_JOB, "Property 'user' should be of type string or null.");
        }
        if (obj.user != null) {
            self.user = obj.user;
        }

        //
        // parse created
        //
        if (type.isString(obj.created)) {
            obj.created = new Date(obj.created);
        }
        if ((Object.prototype.toString.call(obj.created) !== "[object Date]" || isNaN(obj.created.getTime())) && typeof obj.created !== "undefined" && obj.created !== null) {
            throw new Error(ERROR_INVALID_JOB, "Property 'created' should be of type string or null.");
        }
        if (typeof obj.created !== "undefined" && obj.created != null) {
            self.created = obj.created;
        }
        else if (typeof obj.created === "undefined") {
            self.created = new Date();
        }

        //
        // parse scheduled
        //
        if (type.isString(obj.scheduled)) {
            obj.scheduled = new Date(obj.scheduled);
        }
        if ((Object.prototype.toString.call(obj.scheduled) !== "[object Date]" || isNaN(obj.scheduled.getTime())) && typeof obj.scheduled !== "undefined" && obj.scheduled !== null) {
            throw new Error(ERROR_INVALID_JOB, "Property 'scheduled' should be of type string or null.");
        }
        if (typeof obj.scheduled !== "undefined" && obj.scheduled != null) {
            self.scheduled = obj.scheduled;
        }

        //
        // parse started
        //
        if (type.isString(obj.started)) {
            obj.started = new Date(obj.started);
        }
        if ((Object.prototype.toString.call(obj.started) !== "[object Date]" || isNaN(obj.started.getTime())) && typeof obj.started !== "undefined" && obj.started !== null) {
            throw new Error(ERROR_INVALID_JOB, "Property 'started' should be of type string or null.");
        }
        if (typeof obj.started !== "undefined" && obj.started != null) {
            self.started = obj.started;
        }

        //
        // parse ended
        //
        if (type.isString(obj.ended)) {
            obj.ended = new Date(obj.ended);
        }
        if ((Object.prototype.toString.call(obj.ended) !== "[object Date]" || isNaN(obj.ended.getTime())) && typeof obj.ended !== "undefined" && obj.ended !== null) {
            throw new Error(ERROR_INVALID_JOB, "Property 'ended' should be of type string or null.");
        }
        if (typeof obj.ended !== "undefined" && obj.ended != null) {
            self.ended = obj.ended;
        }

        //
        // parse description
        //
        if (!type.isString(obj.description) && typeof obj.description !== "undefined" && obj.description !== null) {
            throw new Error(ERROR_INVALID_JOB, "Property 'description' should be of type string or null.");
        }
        if (obj.description != null) {
            self.description = obj.description;
        }

        //
        // parse parent
        //
        if (!(obj.parent instanceof Job) && typeof obj.parent !== "undefined" && obj.parent !== null) {
            throw new Error(ERROR_INVALID_JOB, "Property 'parent' should be of type Job or null.");
        }
        if (obj.parent != null) {
            self.parent = obj.parent;
        }

        //
        // parse origin
        //
        if (!type.isObject(obj.origin) && typeof obj.origin !== "undefined" && obj.origin !== null) {
            throw new Error(ERROR_INVALID_JOB, "Property 'origin' should be of type object or null.");
        }
        if (obj.origin != null) {
            self.origin = obj.origin;
        }

        //
        // parse tasks
        //
        if (!type.isArray(obj.tasks) && typeof obj.tasks !== "undefined" && obj.tasks !== null) {
            throw new Error(ERROR_INVALID_JOB, "Property 'tasks' should be of type array or null.");
        }
        if (obj.tasks != null) {
            if (obj.tasks.length > 0 && obj.tasks[0].ready !== false) {
                obj.tasks[0].ready = true;
            }

            for (var t in obj.tasks) {
                if (typeof obj.tasks[t].created === "undefined") {
                    obj.tasks[t].created = self.created;
                }
                
                obj.tasks[t] = new Task(obj.tasks[t]);
            }

            self.tasks = obj.tasks;
        }

        //
        // parse parameters
        //
        if (!type.isObject(obj.parameters) && typeof obj.parameters !== "undefined" && obj.parameters !== null) {
            throw new Error(ERROR_INVALID_JOB, "Property 'parameters' should be of type object or null.");
        }
        if (obj.parameters != null) {
            self.parameters = obj.parameters;
        }
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = Job;
module.exports.ERROR_INVALID_JOB = ERROR_INVALID_JOB;